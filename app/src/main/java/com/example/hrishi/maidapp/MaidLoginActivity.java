package com.example.hrishi.maidapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrishi.maidapp.data.DataHelper;

import static com.example.hrishi.maidapp.data.DataContract.DataEntry.Table_Maid_Name;

public class MaidLoginActivity extends AppCompatActivity {

    public EditText etEmail, etPassword;
    public Button bLogin;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maid_login);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bLogin = (Button) findViewById(R.id.bLogin);

        TextView tvRegisterHere = (TextView) findViewById(R.id.tvRegisterHere);
        tvRegisterHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaidLoginActivity.this, MaidRegActivity.class);
                startActivity(intent);
            }
        });

    }

    public void maidLogin(View v) {
        if (v.getId() == R.id.bLogin) {
            MaidDashBoard m=new MaidDashBoard();
            username = etEmail.getText().toString();
            String pass = etPassword.getText().toString();

            String password = searchPass1(username);
            int pass_id=search_id(username);

            if (pass.equals(password)) {

                Toast.makeText(MaidLoginActivity.this, pass_id + "", Toast.LENGTH_SHORT).show();

                Intent i=new Intent(MaidLoginActivity.this,MaidDashBoard.class);
                i.putExtra("pass_id", pass_id);
                startActivity(i);
            } else {
                Toast.makeText(MaidLoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String searchPass1(String uname) {

        DataHelper mDbHelper = new DataHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String query = "select Email,Password from " + Table_Maid_Name;
        Cursor cursor = db.rawQuery(query, null);
        String a, b = "Not Found";

        if (cursor.moveToFirst()) {
            do {
                a = cursor.getString(0);

                if (a.equals(uname)) {
                    b = cursor.getString(1);
                    break;
                }
            } while (cursor.moveToNext());
        }
        return b;

    }

    public int search_id(String username)
    {
        DataHelper mdb = new DataHelper(getApplicationContext());
        SQLiteDatabase db1 = mdb.getReadableDatabase();
        String query = "select _Id,Email from " +Table_Maid_Name;
        String a;
        int b = -1;
        Cursor cu = db1.rawQuery(query,null);
        if(cu.moveToFirst())
        {
            do {
                a = cu.getString(1);
                if(a.equals(username))
                {
                    b = cu.getInt(0);
                    break;
                }
            } while(cu.moveToNext());
        }
        return b;
    }

}

