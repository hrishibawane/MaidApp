package com.example.hrishi.maidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class UserFiltersPage extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String[] region = {"Peth Area", "Kothrud", "Kondhwa", "Katraj", "Shivajinagar", "Bavdhan", "Camp"};
    public String userRegion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_filters_page);

        Spinner regionSpinner = (Spinner) findViewById(R.id.sRegion);
        regionSpinner.setOnItemSelectedListener(this);

        ArrayAdapter regionArray = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, region);
        regionArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        regionSpinner.setAdapter(regionArray);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        userRegion = region[position];
        TextView dispMaids = (TextView) findViewById(R.id.dispMaids);
        dispMaids.setText(userRegion);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
