package com.example.hrishi.maidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class UserType extends AppCompatActivity {

    public RadioButton customerOption, maidOption;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);

    }

    public void nextActivity(View v) {

        customerOption = (RadioButton) findViewById(R.id.custOption);
        maidOption = (RadioButton) findViewById(R.id.maidOption);

        if(customerOption.isChecked()) {

            Toast.makeText(UserType.this, "Hello Customer", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(UserType.this, UserLoginActivity.class);
            startActivity(intent);
        }
        else if(maidOption.isChecked()) {

            Toast.makeText(UserType.this, "Hello Maid", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(UserType.this, MaidLoginActivity.class);
            startActivity(intent);

        }
        else {

            Toast.makeText(UserType.this, "Please Select Any 1 Option", Toast.LENGTH_SHORT).show();
        }
    }

}
