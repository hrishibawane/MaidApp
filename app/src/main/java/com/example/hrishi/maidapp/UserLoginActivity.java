package com.example.hrishi.maidapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrishi.maidapp.data.DataHelper;

import static com.example.hrishi.maidapp.data.DataContract.DataEntry.Table_Name;

public class UserLoginActivity extends AppCompatActivity {

    DataHelper dbhelper = new DataHelper(this);
    public EditText etEmail, etPassword;
    public Button bLogin;
    public TextView tvRegisterHere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        tvRegisterHere = (TextView) findViewById(R.id.tvRegisterHere);

        tvRegisterHere.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i=new Intent(UserLoginActivity.this,UserRegActivity.class);
                startActivity(i);
            }

        });
    }

    public void userLogin(View v) {
        if(v.getId()==R.id.bLogin)
        {
            String username = etEmail.getText().toString();
            String pass = etPassword.getText().toString();
            String password = searchPass(username);

            if(pass.equals(password)) {
                Toast.makeText(UserLoginActivity.this,"Success",Toast.LENGTH_LONG).show();
                Intent i=new Intent(UserLoginActivity.this,UserDashboard.class);
                startActivity(i);
            }
            else
            {
                Toast.makeText(UserLoginActivity.this,"Failed",Toast.LENGTH_LONG).show();
            }
        }
    }

    public String searchPass(String uname) {

        DataHelper mDbHelper1 = new DataHelper(this);
        SQLiteDatabase db1 = mDbHelper1.getReadableDatabase();
        String query = "select Email,Password from " +Table_Name;
        Cursor cursor = db1.rawQuery(query,null);
        String a, b = "Not Found";

        if(cursor.moveToFirst()) {
            do {
                a = cursor.getString(0);

                if(a.equals(uname))
                {
                    b = cursor.getString(1);
                    break;
                }
            } while(cursor.moveToNext());
        }

        cursor.close();
        return b;
    }
}


