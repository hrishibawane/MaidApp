package com.example.hrishi.maidapp;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrishi.maidapp.data.DataContract;
import com.example.hrishi.maidapp.data.DataHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class UserRegActivity extends AppCompatActivity {

    DataHelper mDbHelper=new DataHelper(this);

    private static final String TAG = "UserRegActivity";
    private static final int ERROR_DIALOG_REQUEST = 9001;
    public static String addressLine;
    public static int STATIC_INTEGER_VALUE = 1;

    EditText etName, etEmail, etMobile, etPassword, etCPassword;
    TextView etAddress;
    Button bRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reg);

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etAddress = (TextView) findViewById(R.id.etAddress);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etSetPass);
        etCPassword=(EditText) findViewById(R.id.etConfPass);
        bRegister = (Button) findViewById(R.id.bRegister);

        etAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (servicesOK()) {
                    Intent intent = new Intent(UserRegActivity.this, MapActivity.class);
                    startActivityForResult(intent, STATIC_INTEGER_VALUE);
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == STATIC_INTEGER_VALUE) {
            if(resultCode == RESULT_OK) {

                addressLine = data.getStringExtra("addressLine");
                etAddress.setText(addressLine);

            }
        }
    }

    public void regComplete(View v) {

        if(etName.getText().toString().trim().length() == 0 || etEmail.getText().toString().trim().length() == 0 ||
                etAddress.getText().toString().trim().length() == 0 || etMobile.getText().toString().trim().length() == 0 ||
                etPassword.getText().toString().trim().length() == 0) {

            Toast.makeText(UserRegActivity.this, "Please Fill in all details", Toast.LENGTH_SHORT).show();
        }
        else if(!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
            Toast.makeText(UserRegActivity.this, "Password don't match", Toast.LENGTH_SHORT).show();
        }
        else {
            insetData();
            finish();
        }
    }

    public void insetData()
    {
        String namestr = etName.getText().toString();
        String emailstr = etEmail.getText().toString();
        String addressstr= etAddress.getText().toString();
        String mobilestr = etMobile.getText().toString();
        String passwordstr = etPassword.getText().toString();
        String cpasswordstr= etCPassword.getText().toString();

        DataHelper mDbHelper=new DataHelper(this);
        SQLiteDatabase db=mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataContract.DataEntry.Col_1, namestr);
        values.put(DataContract.DataEntry.Col_2, emailstr);
        values.put(DataContract.DataEntry.Col_3, mobilestr);
        values.put(DataContract.DataEntry.Col_4, addressstr);
        values.put(DataContract.DataEntry.Col_5, passwordstr);

        long newRowId=db.insert(DataContract.DataEntry.Table_Name,null,values);

        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(this, "Error with saving data", Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(this, "Data saved with row id: " + newRowId, Toast.LENGTH_SHORT).show();
        }


    }

    public boolean servicesOK() {
        int isAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(UserRegActivity.this);

        if(isAvailable == ConnectionResult.SUCCESS) {
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(isAvailable)) {
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(UserRegActivity.this, isAvailable, ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else {
            Toast.makeText(this, "Cant connect to Google Play Services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

}
