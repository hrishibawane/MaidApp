
package com.example.hrishi.maidapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrishi.maidapp.data.DataContract;
import com.example.hrishi.maidapp.data.DataHelper;

import java.util.ArrayList;

import static com.example.hrishi.maidapp.data.DataContract.DataEntry.Table_Maid_Name;
import static com.example.hrishi.maidapp.data.DataContract.DataEntry.mCol_1;
import static com.example.hrishi.maidapp.data.DataContract.DataEntry.mCol_6;
import static com.example.hrishi.maidapp.data.DataContract.DataEntry.mCol_7;
import static com.example.hrishi.maidapp.data.DataContract.DataEntry.mCol_8;

public class MaidDashBoard extends AppCompatActivity {
    DataHelper mDbHelper=new DataHelper(this);
    MaidLoginActivity log=new MaidLoginActivity();
    SQLiteDatabase db1;
    public static String TAG = "MaidDashBoard";
    public String name1;
    EditText age;
    EditText region;
    EditText city;
    EditText leaves;
    CheckBox optCook,optChild,optElder,optClean;
    CheckBox optMorning,optEvening,optAfternoon,optFullDay,optHalfDay;
    ArrayList<String> timeSlots = new ArrayList<String>();
    ArrayList<String> workCategory = new ArrayList<String>();
    public int pass_id = -1;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maid_dash_board);

        age =(EditText) findViewById(R.id.age);
        region =(EditText) findViewById(R.id.region);
        city =(EditText)findViewById(R.id.city);
        leaves =(EditText) findViewById(R.id.leave);
        optCook=(CheckBox)findViewById(R.id.cooking) ;
        optChild=(CheckBox)findViewById(R.id.child_care) ;
        optElder=(CheckBox)findViewById(R.id.elderly_care) ;
        optClean=(CheckBox)findViewById(R.id.cleaning) ;


        optMorning=(CheckBox)findViewById(R.id.morning) ;
        optEvening=(CheckBox)findViewById(R.id.evening) ;
        optAfternoon=(CheckBox)findViewById(R.id.afternoon) ;
        optFullDay=(CheckBox)findViewById(R.id.full_day) ;
        optHalfDay=(CheckBox)findViewById(R.id.half_day) ;


        Bundle p_id = getIntent().getExtras();
        pass_id = p_id.getInt("pass_id");


        TextView text=(TextView)findViewById(R.id.next_text);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(age.getText().toString().trim().length() == 0 || region.getText().toString().trim().length() == 0 ||
                        city.getText().toString().trim().length() == 0 || leaves.getText().toString().trim().length() == 0 ||
                        (!(optClean.isChecked()) && !(optElder.isChecked()) && !(optChild.isChecked()) && !(optCook.isChecked())) || (!(optAfternoon.isChecked()) && !(optEvening.isChecked()) && !(optMorning.isChecked()) && !(optFullDay.isChecked()) && !(optHalfDay.isChecked()) )) {

                    Toast.makeText(MaidDashBoard.this, "Please Fill in all details", Toast.LENGTH_SHORT).show();
                }
                else {
                    String cur = "MAID";

                    String age_db = age.getText().toString();
                    String leaves_db = leaves.getText().toString();
                    String region_db = region.getText().toString();

                    SQLiteDatabase db1 = mDbHelper.getWritableDatabase();

                    ContentValues values = new ContentValues();
                    values.put(DataContract.DataEntry.mCol_6, age_db);
                    values.put(DataContract.DataEntry.mCol_7, region_db);
                    values.put(DataContract.DataEntry.mCol_8, leaves_db);
                    int count = db1.update(Table_Maid_Name, values, "_id=?", new String[] {pass_id+""});

                    Toast.makeText(MaidDashBoard.this, count + " Updated", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
