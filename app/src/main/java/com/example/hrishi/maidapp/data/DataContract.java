package com.example.hrishi.maidapp.data;

import android.provider.BaseColumns;

    public final class  DataContract {
        private DataContract(){}

        public static final class DataEntry implements BaseColumns {
            public static final String Table_Name = "Login_Members";
            public static final String _Id = BaseColumns._ID;
            public static final String Col_1 = "Name";
            public static final String Col_2 = "Email";
            public static final String Col_3 = "Mobile_No";
            public static final String Col_4 = "Address";
            public static final String Col_5 = "Password";


            public static final String Table_Maid_Name = "Maid_db";
            public static final String _Id1 = BaseColumns._ID;
            public static final String mCol_1 = "Name";
            public static final String mCol_2 = "Email";
            public static final String mCol_3 = "Mobile_No";
            public static final String mCol_4 = "Address";
            public static final String mCol_5 = "Password";
            public static final String mCol_6 = "Age";
            public static final String mCol_7 = "Region";
            public static final String mCol_8 = "leaves";


        }


    }


