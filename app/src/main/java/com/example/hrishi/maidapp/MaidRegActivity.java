package com.example.hrishi.maidapp;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hrishi.maidapp.data.DataContract;
import com.example.hrishi.maidapp.data.DataHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MaidRegActivity extends AppCompatActivity {


    private static final String TAG = "UserRegActivity";
    private static final int ERROR_DIALOG_REQUEST = 9001;
    public static String addressLine;
    public static int STATIC_INTEGER_VALUE = 1;

    EditText etName;
    EditText etEmail;
    EditText etMobile;
    EditText etPassword;
    EditText etCPassword;
    TextView etAddress;
    Button bRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maid_reg);

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etAddress = (TextView) findViewById(R.id.etAddress);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etPassword = (EditText) findViewById(R.id.etSetPass);
        etCPassword=(EditText) findViewById(R.id.etConfPass);
        bRegister = (Button) findViewById(R.id.bRegister);

        etAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (servicesOK()) {
                    Intent intent = new Intent(MaidRegActivity.this, MapActivity.class);
                    startActivityForResult(intent, STATIC_INTEGER_VALUE);
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == STATIC_INTEGER_VALUE) {
            if(resultCode == RESULT_OK) {

                addressLine = data.getStringExtra("addressLine");
                etAddress.setText(addressLine);

            }
        }
    }

    public void maidRegComplete(View v) {
        if(etName.getText().toString().trim().length() == 0 || etEmail.getText().toString().trim().length() == 0 ||
                etAddress.getText().toString().trim().length() == 0 || etMobile.getText().toString().trim().length() == 0 ||
                etPassword.getText().toString().trim().length() == 0) {

            Toast.makeText(MaidRegActivity.this, "Please Fill in all details", Toast.LENGTH_SHORT).show();
        }
        else if(!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
            Toast.makeText(MaidRegActivity.this, "Password don't match", Toast.LENGTH_SHORT).show();
        }
        else {
            insetData1();
            finish();
        }

    }


    public void insetData1()
    {
        String namestr = etName.getText().toString();
        String emailstr = etEmail.getText().toString();
        String addressstr= etAddress.getText().toString();
        String mobilestr = etMobile.getText().toString();
        String passwordstr = etPassword.getText().toString();
        String cpasswordstr= etCPassword.getText().toString();

        DataHelper DbHelper=new DataHelper(this);
        SQLiteDatabase db1=DbHelper.getWritableDatabase();
        ContentValues values1 = new ContentValues();
        values1.put(DataContract.DataEntry.mCol_1, namestr);
        values1.put(DataContract.DataEntry.mCol_2, emailstr);
        values1.put(DataContract.DataEntry.mCol_3, mobilestr);
        values1.put(DataContract.DataEntry.mCol_4, addressstr);
        values1.put(DataContract.DataEntry.mCol_5, passwordstr);
        values1.put(DataContract.DataEntry.mCol_6, "A");
        values1.put(DataContract.DataEntry.mCol_7, "B");
        values1.put(DataContract.DataEntry.mCol_8, "C");

        long newRowId=db1.insert(DataContract.DataEntry.Table_Maid_Name,null,values1);

        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(this, "Error with saving data", Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(this, "Data saved with row id: " + newRowId, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean servicesOK() {
        int isAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MaidRegActivity.this);

        if(isAvailable == ConnectionResult.SUCCESS) {
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(isAvailable)) {
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MaidRegActivity.this, isAvailable, ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else {
            Toast.makeText(this, "Cant connect to Google Play Services", Toast.LENGTH_SHORT).show();
        }
        return false;
    }


}
