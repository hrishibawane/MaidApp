package com.example.hrishi.maidapp.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.NfcAdapter;

import static com.example.hrishi.maidapp.data.DataContract.DataEntry.Table_Maid_Name;
import static com.example.hrishi.maidapp.data.DataContract.DataEntry.Table_Name;

public class DataHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="Maidapp.db";

    public DataHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQL_CREATE_ENTRIES="CREATE TABLE " + Table_Name + "("
                + DataContract.DataEntry._Id + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DataContract.DataEntry.Col_1 + " TEXT NOT NULL, "
                + DataContract.DataEntry.Col_2 + " TEXT NOT NULL, "
                + DataContract.DataEntry.Col_3 + " TEXT NOT NULL, "
                + DataContract.DataEntry.Col_4 + " TEXT NOT NULL, "
                + DataContract.DataEntry.Col_5 + " TEXT  NOT NULL);";
        db.execSQL(SQL_CREATE_ENTRIES);

        String Create_Maid_Table="CREATE TABLE " + Table_Maid_Name + "("
                + DataContract.DataEntry._Id1 + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DataContract.DataEntry.mCol_1 + " TEXT NOT NULL, "
                + DataContract.DataEntry.mCol_2 + " TEXT NOT NULL, "
                + DataContract.DataEntry.mCol_3 + " TEXT NOT NULL, "
                + DataContract.DataEntry.mCol_4 + " TEXT NOT NULL, "
                + DataContract.DataEntry.mCol_5 + " TEXT NOT NULL, "
                + DataContract.DataEntry.mCol_6 + " TEXT , "
                + DataContract.DataEntry.mCol_7 + " TEXT , "
                + DataContract.DataEntry.mCol_8 + " TEXT );";
        db.execSQL(Create_Maid_Table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}